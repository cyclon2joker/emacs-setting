;; 日本語環境設定 
(set-language-environment 'Japanese)
(setq default-input-method 'MacOSX)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(setq auto-coding-functions nil)

;; ロードパス追加
(defun add-to-load-path (&rest paths)
(let (path)
(dolist (path paths paths)
(let ((default-directory
(expand-file-name (concat user-emacs-directory path))))
(add-to-list 'load-path default-directory)
(if (fboundp 'normal-top-level-add-subdirs-to-load-path)
    (normal-top-level-add-subdirs-to-load-path))))))

;; 引数のディレクトリとそのサブディレクトリをload-pathに追加
(add-to-load-path "elisp" "conf" "public_repos")


(if window-system
    (progn
     ;; フォント
      ;; (pp (font-family-list)) でfont-familyの出力可能
      (set-face-attribute 'default nil
                          :height 120 :family "Menlo")
      (set-fontset-font nil 'japanese-jisx0208
                        (font-spec :family "Noto Sans Japanese")))
)

;; バックスラッシュ入力
(define-key global-map [?¥] [?\\])

;; drag-drop-file
(define-key global-map [ns-drag-file] 'ns-find-file)

;; 画面設定
(if window-system
  (setq default-frame-alist
     (append (list
     '(width . 120)
     '(height . 80)
;     '(background-color . "RoyalBlue4")
;     '(foreground-color . "LightGray")
     '(background-color . "Black")
     '(foreground-color . "LightGreen")
     '(cursor-color . "dim gray")
     )
     default-frame-alist))
)     

   ;; 行番号の表示
   (global-linum-mode t)
   (line-number-mode t)

   ;; 列番号の表示
   (column-number-mode t)

   ;; 時刻の表示
   (require 'time)
   (setq display-time-24hr-format t)
   (setq display-time-string-forms '(24-hours ":" minutes))
   (display-time-mode t)

   (defface my-hl-line-face
     ;; 背景がdarkならば背景色を紺に
     '((((class color) (background dark))
        (:background "NavyBlue" t))
       ;; 背景がlightならば背景色を青に
       (((class color) (background light))
        (:background "LightSkyBlue" t))
       (t (:bold t)))
     "hl-line's my face")
(setq hl-line-face 'my-hl-line-face)
(global-hl-line-mode t)





;; ------------------------------------------------------------------------
;; @ cursor

   ;; カーソル点滅表示
   (blink-cursor-mode 0)

   ;; スクロール時のカーソル位置の維持
   (setq scroll-preserve-screen-position t)

   ;; スクロール行数（一行ごとのスクロール）
   (setq vertical-centering-font-regexp ".*")
   (setq scroll-conservatively 35)
   (setq scroll-margin 0)
   (setq scroll-step 1)

   ;; 画面スクロール時の重複行数
   (setq next-screen-context-lines 1)

;;-------------------------------------------
;; @ backup設定
   ;; 変更ファイルのバックアップ
   (setq make-backup-files nil)
   ;; 変更ファイルの番号つきバックアップ
   (setq version-control nil)
   ;; 編集中ファイルのバックアップ
   (setq auto-save-list-file-name nil)
   (setq auto-save-list-file-prefix nil)

   ;; 編集中ファイルのバックアップ先
   (setq auto-save-file-name-transforms
         `((".*" ,temporary-file-directory t)))

   ;; 編集中ファイルのバックアップ間隔（秒）
   (setq auto-save-timeout 30)

   ;; 編集中ファイルのバックアップ間隔（打鍵）
   (setq auto-save-interval 500)

   ;; バックアップ世代数
   (setq kept-old-versions 1)
   (setq kept-new-versions 2)


;; ------------------------------------------------------------------------
;; @ tab
;;タブ幅を 2 に設定
(setq-default tab-width 2)
;;タブ幅の倍数を設定
(setq tab-stop-list
  '(2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60))
;;タブではなくスペースを使う
(setq-default indent-tabs-mode nil)
(setq indent-line-function 'indent-relative-maybe)

;; ------------------------------------------------------------------------
;; C-x <C-left/right>でバッファの切り替え（これはデフォルト動作）
;; C-x <C-up/down>でフレームの切り替え
;;(global-set-key [?\C-x M-up] '(lambda () "" (interactive) (other-frame -1)))
;;(global-set-key [?\C-x M-down] '(lambda () "" (interactive) (other-frame 1)))
(global-set-key [M-up] '(lambda () "" (interactive) (other-frame -1)))
(global-set-key [M-down] '(lambda () "" (interactive) (other-frame 1)))

;; ------------------------------------------------------------------------

;; elpa
;; (default t)
;;(dotspacemacs-elpa-https t)
;; Maximum allowed time in seconds to contact an ELPA repository.
;; (default 5)
;;(dotspacemacs-elpa-timeout 5)


(package-initialize)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)

;; auto-install
(add-to-list 'load-path (expand-file-name "~/.emacs.d/auto-install/"))
(require 'auto-install)
(auto-install-update-emacswiki-package-name t)
(auto-install-compatibility-setup)


(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; svn
(require 'psvn)
(setq process-coding-system-alist '(("svn" . utf-8)))
(setq default-file-name-coding-system 'utf-8)
(setq svn-status-svn-file-coding-system 'utf-8)







